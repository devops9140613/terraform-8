terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "2.21.0"
    }
  }
}


provider "aws" {
  access_key = var.accesskey
  secret_key = var.secretkey
  region = "eu-west-1"
}

provider "digitalocean" {
  token = var.do_token
}

provider "local"{
}
