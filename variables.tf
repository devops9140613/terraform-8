variable "do_token" {
  type = string
}

variable "sshkey" {
  type = string
}

variable "accesskey"{
  type = string
}

variable "secretkey"{
  type = string
}

variable "pass" {
  type = string
}

variable "devs" {
  type = list(string)
  default =  ["vsp1-imslttan-at-yandex-ru", "app1-imslttan-at-yandex-ru", "app2-imslttan-at-yandex-ru"]
  }


variable "mail" {
  type = string
}

variable "task" {
  type = string
}
